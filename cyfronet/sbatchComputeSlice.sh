#!/bin/bash
if [ "$#" -ne 2 ]; then
  echo "Invalid invocation."
  exit 1
fi

if [ "$1" -gt "$2" ]; then
  echo "Error: start > end."
  exit 1
fi

sbatch \
  -N 1 \
  --cpus-per-task=24 \
  --mem=120GB \
  -p plgrid \
  --time=05:29:00 \
  -J "cOm_$1_$2" \
  -A dyskrepancje2 \
  --output="$PLG_USER_STORAGE/output/stdout_$1_$2" \
  --error="$PLG_USER_STORAGE/error/stderr_$1_$2" \
  runComputeSlice.sh $1 $2
