#!/bin/bash -l

module load plgrid/tools/gcc/7.1.0

cd $PLG_USER_STORAGE/lopeks-master-thesis
g++ verifyPartialCounts.cpp -o $TMPDIR/verifyPartialCounts -std=c++1y  -pthread -O3 -Wall

cd $SCRATCH
time $TMPDIR/verifyPartialCounts 3332 $1
