#!/bin/bash
if [ "$#" -ne 2 ]; then
  echo "Invalid invocation."
  exit 1
fi

if [ "$1" -gt "$2" ]; then
  echo "Error: start > end."
  exit 1
fi

sbatch \
  -N 1 \
  --cpus-per-task=24 \
  --mem=110GB \
  -p plgrid \
  --time=2:59:00 \
  -J "col_$1_$2" \
  -A dyskrepancje2 \
  --output="$PLG_USER_STORAGE/output/collectCounts/stdout_$1_$2" \
  --error="$PLG_USER_STORAGE/error/collectCounts/stderr_$1_$2" \
  runCollect.sh $1 $2
