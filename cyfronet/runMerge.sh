#!/bin/bash -l

module load plgrid/tools/gcc/7.1.0

cd $PLG_USER_STORAGE/lopeks-master-thesis
g++ mergePartialCounts.cpp -o $TMPDIR/mergePartialCounts -std=c++1y -pthread -O3 -Wall

cd $SCRATCH
time $TMPDIR/mergePartialCounts 3332 $PLG_GROUPS_STORAGE/plggerdos/m$1 /net/scratch/people/plglopek/counts/$1/*
