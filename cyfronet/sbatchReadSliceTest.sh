#!/bin/bash

sbatch \
  -N 1 \
  --cpus-per-task=1 \
  --mem=20GB \
  -p plgrid \
  --time=00:40:00 \
  -J "readSlice" \
  -A dyskrepancje2 \
  --output="$PLG_USER_STORAGE/output/readSlice" \
  --error="$PLG_USER_STORAGE/error/readSlice" \
  runReadSliceTest.sh
