#include "common.h"
#include "constants.h"

#include <atomic>
#include <cassert>
#include <cstdio>
#include <queue>
#include <vector>
#include <memory>

class SliceComputation {
 private:
  int64_t* slice_;
  const int sliceId_;
  const std::vector<int32_t>& primes_;
  
  const int64_t sliceStart_, sliceEnd_;

 public:
  SliceComputation(
      int64_t* slice,
      int sliceId,
      const std::vector<int32_t>& primes)
    : slice_(slice),
      sliceId_(sliceId),
      primes_(primes),
      sliceStart_(SLICE_SIZE * sliceId + 1),
      sliceEnd_(SLICE_SIZE * (sliceId + 1)) {

    if (sliceId == 0) {
      computeSlice0();
    }
  }

  int getMaxSliceNecessary() {
    for (int otherId = 0; ; otherId++) {
      if (!shouldProcessSlice(otherId)) {
        return otherId - 1;
      }
    }
  }

  bool shouldProcessSlice(int otherId) {
    if (sliceId_ == 0) {
      return false;
    }
    int64_t otherStart = SLICE_SIZE * otherId + 1;
    return otherStart * 11 <= sliceEnd_;
  }

  void processSlice(int otherId, const int64_t* other) {
    Stopwatch stopwatch(
        "Processing slice " + toString(otherId) + " by slice " + toString(sliceId_));

    if (!shouldProcessSlice(otherId)) {
      return;
    }

    int64_t otherStart = SLICE_SIZE * otherId + 1;
    int64_t otherEnd = otherStart + SLICE_SIZE - 1;

    std::atomic<int> nextCi(0);
    auto worker = [&]() {
      while (true) {
        int ci = nextCi.fetch_add(1);
        if (ci >= SLICE_SIZE / CHUNK_SIZE) {
          break;
        }
        computeSliceChunk(
            slice_ + WORDS_IN_CHUNK * ci,
            sliceStart_ + CHUNK_SIZE * ci,
            sliceStart_ + CHUNK_SIZE * (ci + 1) - 1,
            other,
            otherStart,
            otherEnd);
      }
    };
    runMultiThreaded(worker);
  }

 private:
  void computeSlice0() {
    Stopwatch stopwatch("Computing slice 0");
    slice_[0] = 0b0000010100000010100001000100000100001000000000000000000000000001;
    slice_[1] = 0b1010011100100010010100001000100000110010001001000010011000010100;
    slice_[2] = 0b0000010011010110010000010100101010100011000100001000100001010000;

    std::atomic<int64_t> curSize(840);
    while (curSize < CHUNK_SIZE) {
      const int64_t limit = std::min(curSize * 11, SLICE_SIZE);
      computeSliceChunk(
          slice_ + (curSize / SEGMENT_LEN) * WORDS_IN_SEGMENT,
          curSize + 1,
          limit,
          slice_,
          1,
          curSize);
      curSize.store(limit);
    }

    while (curSize < SLICE_SIZE) {
      const int64_t limit = std::min(curSize * 11, SLICE_SIZE);
      auto worker = [&]() {
        while (true) {
          int64_t start = curSize.fetch_add(CHUNK_SIZE) + 1;
          int64_t end = std::min(start + CHUNK_SIZE - 1, limit);
          if (start > limit) {
            break;
          }
          computeSliceChunk(
              slice_ + (start / SEGMENT_LEN) * WORDS_IN_SEGMENT,
              start,
              end,
              slice_,
              1,
              start - 1);
        }
      };
      runMultiThreaded(worker);
      curSize.store(limit);
    }
  }

  void computeSliceChunk(
      int64_t* newSlice,
      int64_t newStart,
      int64_t newEnd,
      const int64_t* other,
      int64_t otherStart,
      int64_t otherEnd) {

    for (size_t pi = 4; pi < primes_.size(); pi++) {
      int64_t p = primes_[pi];

      int64_t n = std::max(otherStart * p, newStart);
      if (n > newEnd) {
          // It's not just an optimization, it's corectness. We don't want
          // otherStart * p to overflow 64 bits.
          break;
      }
      n -= n % p;
      while (n < newStart || n % 2 == 0 || !SEMIPRIME357[n % 105]) {
        n += p;
      }

      //int64_t end = std::min(otherEnd * p, newEnd);
      int64_t end = newEnd;
      if (p < 9223372036854775807ll / otherEnd) {
        end = std::min(otherEnd * p, end);
      }

      int64_t ndivIdx = INDEX(n / p - otherStart + 1);
      while (n <= end) {
        if (SEMIPRIME357[n % 105]) {
          int64_t k = ndivIdx / 64;
          int64_t j = ndivIdx % 64;
          if (0 == (other[k] & (((uint64_t) 1) << j))) {
            turnOn(newSlice, n - newStart + 1);
          }
          ndivIdx++;
        }
        n += 2 * p;
      }
    }
  }

  void turnOn(int64_t* lambda, int64_t n) {
    int64_t i = INDEX(n);
    int64_t k = i / 64;
    int64_t j = i % 64;
    lambda[k] |= (((uint64_t) 1) << j);
  }
};
