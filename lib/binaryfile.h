#pragma once

#include "common.h"

#include <atomic>
#include <cassert>
#include <fstream>

class BinaryFile {
 private:
   std::string path_;

 public:
  BinaryFile(std::string path) : path_(path) {}

  bool exists() {
    return std::ifstream(path_.c_str()).good();
  }

  void truncate() {
    std::ofstream file;
    file.open(path_.c_str(), std::ofstream::out | std::ofstream::trunc);
    assert(file.is_open());
    assert(file.good());
    file.close();
  }

  void write(const char* const arr, int64_t len) {
    Stopwatch stopwatch("Writing to " + path_);
    std::ofstream file;
    file.open(path_.c_str(), std::ios::out | std::ios::binary);
    if (!file.is_open()) {
      printf("Failed to open file %s.\n", path_.c_str());
      abort();
    }
    file.write(arr, len);
    assert(file.good());
    file.close();
  }

  void read(char* arr, int64_t len) {
    Stopwatch stopwatch("Reading from " + path_, true);

    std::atomic<int64_t> fPart(0);
    const int64_t threadsCount =
      len >= 16ll * 1024 * 1024 ? std::max(NUMBER_OF_THREADS - 1, (int64_t) 1) : 1;

    auto worker = [&]() {
      int64_t p = fPart.fetch_add(1);
      int64_t start = (len * p) / threadsCount;
      int64_t end = (len * (p + 1)) / threadsCount;
      readFilePart(arr + start, start, end - start, end == len);
    };
    runMultiThreaded(worker, threadsCount);
  }

 private:
  void readFilePart(char* arr, int64_t start, int64_t len, bool reachesEof) {
    std::ifstream file;
    file.open(path_.c_str(), std::ios::in | std::ios::binary);
    if (!file.is_open()) {
      printf("Failed to open file %s.\n", path_.c_str());
      abort();
    }
    file.seekg(start, file.beg);
    file.read(arr, len);
    assert(file.gcount() == len);
    assert(!file.eof());

    if (reachesEof) {
      char oneMore;
      file.read(&oneMore, 1);
      assert(file.eof());
      file.close();
    }
  }
};
