#include "lib/binaryfile.h"

#include <set>

using namespace std;

int main(int argc, const char* argv[]) {
  if (argc != 5 || atoi(argv[1]) <= 0 || atoi(argv[2]) <= 0 || atoi(argv[3]) <= 0) {
    printf("Usage: %s sample_interval total_samples plot_datapoints merged_counts_file\n", argv[0]);
    return 1;
  }

  const int64_t sampleInterval = atoi(argv[1]);
  const int64_t totalSamples = atoi(argv[2]);
  const int64_t plotDatapoints = atoi(argv[3]);
  string fileName(argv[4]);
  assert(totalSamples >= plotDatapoints);
  assert(sampleInterval % 840 == 0);

  int32_t* samples = new int32_t[totalSamples];
  BinaryFile file(fileName);
  file.read((char*) samples, totalSamples * 4);
  
  const int64_t range = sampleInterval * totalSamples;
  vector<int64_t> minv(plotDatapoints, range);
  vector<int64_t> maxv(plotDatapoints, -range);
  
  int64_t* fsamples = new int64_t[totalSamples];
  fsamples[0] = samples[0] - (sampleInterval - samples[0]);
  for (int64_t i = 1; i < totalSamples; i++) {
    fsamples[i] = fsamples[i - 1] + samples[i] - (sampleInterval - samples[i]);
  }

  int64_t j = 0;
  for (int64_t i = 0; i < plotDatapoints; i++) {
    int64_t limit = (range + plotDatapoints - 1) / plotDatapoints * (i + 1);
    while ((j + 1) * sampleInterval < limit) {
      minv[i] = min(minv[i], fsamples[j]);
      maxv[i] = max(maxv[i], fsamples[j]);
      j++;
    }
    printf("%ld %ld %ld\n", limit, minv[i], maxv[i]);
  }

  return 0;
}
