all:
	mkdir bin/ -p
	g++ computeSlice.cpp -o bin/computeSlice -std=c++1y -pthread -O3 -Wall
	g++ verifySlice.cpp -o bin/verifySlice -std=c++1y -pthread -O3 -Wall
	g++ collectCounts.cpp -o bin/collectCounts -std=c++1y  -pthread -O3 -Wall
	g++ verifyPartialCounts.cpp -o bin/verifyPartialCounts -std=c++1y -pthread -O3 -Wall
	g++ mergePartialCounts.cpp -o bin/mergePartialCounts -std=c++1y -pthread -O3 -Wall
	g++ verifier.cpp -o bin/verifier -std=c++11 -pthread -O3 -Wall
	g++ preparePlotData.cpp -o bin/preparePlotData -std=c++11 -pthread -O3 -Wall
	g++ liouvilleZeros.cpp -o bin/liouvilleZeros -std=c++11 -pthread -O3 -Wall
	g++ approximate.cpp -o bin/approximate -std=c++11 -pthread -O3 -Wall
