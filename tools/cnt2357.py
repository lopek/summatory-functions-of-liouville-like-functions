#!/usr/bin/python

maxN = 10 ** 15

Q = [1]
count = 0
while len(Q) > 0:
    k = Q[0]
    Q = Q[1:]
    if k > maxN:
        continue
    count += 1
    Q.append(k * 7)
    if k % 7 != 0:
        Q.append(k * 5)
        if k % 5 != 0:
            Q.append(k * 3)
            if k % 3 != 0:
                Q.append(k * 2)
print count
