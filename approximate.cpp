#include "lib/binaryfile.h"

#include <cmath>
#include <algorithm>

using namespace std;

const int64_t totalDatapoints = 1000000;

int main(int argc, const char* argv[]) {
  if (argc != 4 || atoi(argv[1]) <= 0 || atoi(argv[2]) <= 0) {
    printf("Usage: %s sample_interval total_samples merged_counts_file\n", argv[0]);
    return 1;
  }

  const int64_t sampleInterval = atoi(argv[1]);
  const int64_t totalSamples = atoi(argv[2]);
  string fileName(argv[3]);
  assert(sampleInterval % 840 == 0);

  int32_t* samples = new int32_t[totalSamples];
  BinaryFile file(fileName);
  file.read((char*) samples, totalSamples * 4);

  int64_t* fsamples = new int64_t[totalSamples];
  fsamples[0] = samples[0] - (sampleInterval - samples[0]);
  for (int64_t i = 1; i < totalSamples; i++) {
    fsamples[i] = fsamples[i - 1] + samples[i] - (sampleInterval - samples[i]);
  }
  delete[] samples;

  long double A = 0, B = 0;
  for (int64_t i = 0; i < totalSamples; i++) {
    int64_t x = sampleInterval * (i + 1);
    int64_t y = fsamples[i];

    A += sqrtl(x) * y;
    B += x;
  }

  long double c = A / B, errSum = 0;
  vector<double> errors;
  for (int64_t i = 0; i < totalSamples; i++) {
    int64_t x = sampleInterval * (i + 1);
    int64_t y = fsamples[i];
    errors.push_back(abs(c * sqrtl(x) - y) / abs(y));
    errSum += errors.back();
  }

  sort(errors.begin(), errors.end());

  printf("c = %Lf\n", c);
  printf("Avg error: %Lf\n", errSum / errors.size());
  printf("P10 error: %lf\n", errors[(errors.size() * 10) / 100]);
  printf("P50 error: %lf\n", errors[errors.size() / 2]);
  printf("P95 error: %lf\n", errors[(errors.size() * 95) / 100]);
  printf("P99 error: %lf\n", errors[(errors.size() * 99) / 100]);
  printf("Max error: %lf\n", errors.back());

  delete[] fsamples;
  return 0;
}
