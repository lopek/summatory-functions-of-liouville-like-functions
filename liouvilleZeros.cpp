#include "lib/binaryfile.h"
#include "lib/common.h"
#include "lib/computelambda.h"

#include <atomic>
#include <set>

using namespace std;

void reportZeros(int64_t nStart, int64_t vStart, int64_t nEnd) {
  vector<int64_t> res;
  int64_t n = nStart, v = vStart;
  if (v == 0) {
    res.push_back(n);
  }

  while (n < nEnd) {
    n++;
    v += lambda(n) ? 1 : -1;
    if (v == 0) {
      res.push_back(n);
    }
    if (abs(v) > nEnd - n) {
      break;
    }
  }

  Printf ptf;
  ptf.printf("# Found %lu roots starting from %ld.\n", res.size(), nStart);
  for (int64_t n : res) {
    ptf.printf("%ld\n", n);
  }
}

bool canReachZero(int64_t prevEndSum, int64_t endSum, int64_t interval) {
  return abs(prevEndSum) + abs(endSum) <= interval;
}

bool likelyReachesZero(int64_t prevEndSum, int64_t endSum) {
  return min(abs(prevEndSum), abs(endSum)) < 1500;
}

int main(int argc, const char* argv[]) {
  if (argc != 4 || atoi(argv[1]) <= 0 || atoi(argv[2]) <= 0) {
    printf("Usage: %s sample_interval total_samples merged_counts_file\n", argv[0]);
    return 1;
  }

  const int64_t sampleInterval = atoi(argv[1]);
  const int64_t totalSamples = atoi(argv[2]);
  string fileName(argv[3]);
  assert(sampleInterval % 840 == 0);

  int32_t* samples = new int32_t[totalSamples];
  BinaryFile file(fileName);
  file.read((char*) samples, totalSamples * 4);
  
  int64_t* fsamples = new int64_t[totalSamples];
  fsamples[0] = samples[0] - (sampleInterval - samples[0]);

  for (int64_t i = 1; i < totalSamples; i++) {
    fsamples[i] = fsamples[i - 1] + samples[i] - (sampleInterval - samples[i]);
  }
  delete[] samples;

  int64_t prevSum = 0;
  int64_t cntLo = 0, cntMid1 = 0, cntMid2 = 0, cntMid3 = 0, cntHi = 0;
  vector<pair<int64_t, int64_t>> intervalsToCheck;
  for (int64_t i = 0; i < totalSamples; i++) {
    int64_t sampleBeg = i * sampleInterval + 1;
    int64_t sampleEnd = sampleBeg + sampleInterval - 1;

    if (sampleBeg <  200ll * 1000ll * 1000ll * 1000ll * 1000ll) {
      if (canReachZero(prevSum, fsamples[i], sampleInterval)) cntLo++;
    }

    if (sampleEnd >= 200ll * 1000ll * 1000ll * 1000ll * 1000ll &&
        sampleBeg <  350ll * 1000ll * 1000ll * 1000ll * 1000ll) {
      if (canReachZero(prevSum, fsamples[i], sampleInterval)) cntMid1++;
    }

    if (sampleEnd >= 350ll * 1000ll * 1000ll * 1000ll * 1000ll &&
        sampleBeg <  354ll * 1000ll * 1000ll * 1000ll * 1000ll) {
      if (canReachZero(prevSum, fsamples[i], sampleInterval)) cntMid2++;
      if (likelyReachesZero(prevSum, fsamples[i])) {
        intervalsToCheck.push_back({sampleBeg - 1, prevSum});
      }
    }

    if (sampleEnd >= 354ll * 1000ll * 1000ll * 1000ll * 1000ll &&
        sampleBeg <  357ll * 1000ll * 1000ll * 1000ll * 1000ll) {
      if (canReachZero(prevSum, fsamples[i], sampleInterval)) cntMid3++;
    }

    if (sampleEnd >= 357ll * 1000ll * 1000ll * 1000ll * 1000ll) {
      if (canReachZero(prevSum, fsamples[i], sampleInterval)) cntHi++;
    }
    prevSum = fsamples[i];
  }
  printf("[1, 2 * 10^14)               - %ld\n", cntLo);
  printf("[2 * 10^14, 3.5 * 10^14)     - %ld\n", cntMid1);
  printf("[3.5 * 10^14, 3.54 * 10^14)  - %ld\n", cntMid2);
  printf("[3.54 * 10^14, 3.57 * 10^14) - %ld\n", cntMid3);
  printf("[3.57 * 10^14, 10^15]        - %ld\n", cntHi);
  printf("Will check %lu samples.\n", intervalsToCheck.size());

  atomic<size_t> sampleIdx(0);
  auto worker = [&]() {
    while (true) {
      size_t i = sampleIdx.fetch_add(1);
      if (i >= intervalsToCheck.size()) {
        break;
      }
      int64_t nStart = intervalsToCheck[i].first;
      int64_t vStart = intervalsToCheck[i].second;
      int64_t nEnd = nStart + sampleInterval;
      reportZeros(nStart, vStart, nEnd);
    }
  };
  runMultiThreaded(worker);

  delete[] fsamples;
  return 0;
}
